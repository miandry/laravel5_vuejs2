import Vue from 'vue'
import Router from 'vue-router'
import VAnimateCss from 'v-animate-css';
import BaiduMap from 'vue-baidu-map'

import Home from '@/components/Home.vue';
import Details from '@/components/Details.vue';
import ViewMap from '@/components/ViewMap.vue';

Vue.use(BaiduMap, {
  /* Visit http://lbsyun.baidu.com/apiconsole/key for details about app key. */
  ak: 'hGqUKAq9TUVP3EyMRa9lKADfSLb3QrAq'
});
Vue.use(VAnimateCss);
Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', component: Home  },
    { path: '/home', component: Home  },
    { path: '/viewmap', component: ViewMap  },
    { path: '/details/:id/:distance', component: Details },
  ]
})
