<?php
namespace App\Library ;

use App\Starbucks;

class StarbuckUtility
    {
        /**
         * Mean raidus of the earth in kilometers.
         * @var double
         */
        const RADIUS    = 6372.797;

        /**
         * Pi divided by 180 degrees. Calculated with PHP Pi constant.
         * @var double
         */
        const PI180         = 0.017453293;

        /**
         * Constant for converting kilometers into miles.
         * @var double
         */
        const MILES     = 0.621371192;

        /**
         * Calculate distance between two points of latitude and longitude.
         * @param double $lat1 The first point of latitude.
         * @param double $long1 The first point of longitude.
         * @param double $lat2 The second point of latitude.
         * @param double $long2 The second point of longitude.
         * @param bool $kilometers Set to false to return in miles.
         * @return double The distance in kilometers or miles, whichever selected.
         */
        public static function getDistance($lat1, $long1, $lat2, $long2, $kilometers = true)
        {
            $lat1   *= self::PI180;
            $long1  *= self::PI180;
            $lat2   *= self::PI180;
            $long2  *= self::PI180;

            $dlat = $lat2 - $lat1;
            $dlong = $long2 - $long1;

            $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlong / 2) * sin($dlong / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

            $km = self::RADIUS * $c;

            if($kilometers)
            {
                return $km;
            }
            else
            {
                return $km * self::MILES;
            }
        }

        public static function getNearestLocation($circle=5,$latitude,$longitude,$variables){
            $items=[];
            foreach ($variables as $key => $value) {
                $distance = StarbuckUtility::getDistance($latitude, $longitude, $value['latitude'],$value['longitude']);
                
                if( $distance <  $circle ){
                     $value['distance']= round($distance,2) ;
                     $items[] =  $value ;
                }
            }
            return $items ;
        }
        //Fake places
        public static function randomLocation(){
                $locations =  Starbucks::all();
                $items=[];
                $i=0;
                foreach ($locations as $key => $value) {
                      if($i<6){
                        $items[]=array(
                         'latitude'=>$value['latitude'],
                         'longitude'=>$value['longitude'],
                        );
                     }else{
                        break;
                     }

                         $i++ ;
                }
                return  $items ;
        }

    }

