<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Starbucks;
use App\Http\Requests;
use App\Library\StarbuckUtility;

class StarbucksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
            $array_var = StarbuckUtility::randomLocation();

            $variables['random'] =   $array_var[array_rand($array_var,1)];

            return view('starbuck.index',['variables' => $variables]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        // filter by city
        if(isset($request->city)){
           $starbucks =  Starbucks::where('city','=',$city);
        }else{
           $starbucks =  Starbucks::all();  
        }
        
        // filter by user location
        if(isset($request->longitude)&&isset($request->latitude)&&isset($request->circle)){
           $latitude = $request->latitude; 
           $longitude = $request->longitude; 
           $circle = $request->circle; 
           $results = StarbuckUtility::getNearestLocation($circle, $latitude,$longitude,$starbucks);
        }else{
           $results = $starbucks  ;
        }   
        return  response()->json( $results);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $starbucks =  Starbucks::where('id','=',$id)->first();
        return  response()->json($starbucks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
