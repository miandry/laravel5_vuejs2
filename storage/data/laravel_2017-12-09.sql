# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20)
# Database: laravel
# Generation Time: 2017-12-09 09:55:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2017_12_06_144518_create_starbucks_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table starbucks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `starbucks`;

CREATE TABLE `starbucks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(8,2) NOT NULL,
  `longitude` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `starbucks` WRITE;
/*!40000 ALTER TABLE `starbucks` DISABLE KEYS */;

INSERT INTO `starbucks` (`id`, `city`, `street`, `latitude`, `longitude`, `created_at`, `updated_at`)
VALUES
	(817,'New York','1445 First Avenue',40.77,-73.95,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(839,'New York','2252 Broadway',40.78,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(852,'New York','1656 Broadway',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(2784,'New York','1330 6th Ave.',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(2785,'New York','770 Eighth Ave',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(5072,'New York','227 W 27th St',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(5114,'New York','1491 Lexington Avenue',40.79,-73.95,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7215,'New York','1585 Broadway',40.76,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7216,'New York','120 EAST 87TH ST',40.78,-73.96,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7222,'New York','330 Madison Ave',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7238,'New York','1372 Broadway',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7245,'New York','322 West 57th Street',40.77,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7255,'New York','124 Eighth Avenue',40.74,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7263,'New York','684 Eighth Avenue',40.76,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7273,'New York','1100 Avenue of the Americas',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7277,'New York','304 Park Avenue South',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7282,'New York','750 Seventh Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7285,'New York','684 Avenue of the Americas',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7297,'New York','1166 Avenue of the Americas',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7316,'New York','370 7th Avenue',40.75,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7317,'New York','267-275 Columbus Ave',40.78,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7342,'New York','639 3rd Avenue',40.75,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7344,'New York','1841 Broadway',40.77,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7346,'New York','550 Madison Avenue, #A32',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7352,'New York','540 Columbus Avenue',40.79,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7353,'New York','1 Penn Plaza',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7370,'New York','757 Third Avenue',40.75,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7373,'New York','107 E 43rd St, Space MC-72',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7378,'New York','10 UNION SQUARE EAST',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7393,'New York','325 W 49th St',40.76,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7403,'New York','462 7th Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7404,'New York','116 E. 57th Street',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7420,'New York','261 Fifth Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7424,'New York','280 Park Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7426,'New York','575 Fifth Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7427,'New York','424 Park Avenue South',4.07,-73.98,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7443,'New York','30 Rockefeller Plaza, Space A',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7444,'New York','30 Rockefeller Plaza',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7446,'New York','450 7th Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7462,'New York','545 Fifth Ave.',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7463,'New York','1460 Broadway',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7468,'New York','600 Eighth Ave.',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7470,'New York','494 Eighth Ave.',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7501,'New York','871 8th Avenue',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7506,'New York','150 E. 42nd Street',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7511,'New York','525 7th Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7525,'New York','135 East 57th Street',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7536,'New York','200 Madison Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7540,'New York','41 Union Square West',40.74,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7547,'New York','72 Grove Street',40.73,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7575,'New York','685 Third Avenue',40.75,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7586,'New York','177 Eighth Avenue',40.74,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7587,'New York','750 6th Avenue',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7608,'New York','511 Lexington Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7611,'New York','360 Lexington Avenue',40.75,-73.98,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(7622,'New York','145 Third Avenue',40.73,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7655,'New York','630 Lexington Ave',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7670,'New York','825 Eighth Avenue, W-9',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7673,'New York','156 W 52nd St',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7696,'New York','120 W. 56th Street',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7698,'New York','142 W. 57th Street',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7709,'New York','338 Columbus Avenue',40.78,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7711,'New York','76 9th Ave',40.74,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7761,'New York','599 Lexington',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7769,'New York','151 W. 34th Street, Room 900',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7792,'New York','776 Avenue of the Americas',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7801,'New York','295 Madison Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7802,'New York','560 Lexington Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7826,'New York','450 W. 33rd Street',40.75,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7851,'New York','1 Penn Plaza Concourse Level',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7860,'New York','725 5th Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7890,'New York','251 West 42nd Street',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(7898,'New York','125 Park Ave',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8130,'New York','3 Park Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8615,'New York','870 7th Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8769,'New York','1500 Broadway',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8812,'New York','90 Park Ave',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8965,'New York','45 E. 51st Street',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8992,'New York','1710 Broadway',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(8993,'New York','1530 Broadway',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9239,'New York','1290 6th Ave',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9240,'New York','1345 6th Ave',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9241,'New York','55 E 53rd St',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9242,'New York','450 Lexington Avenue',40.75,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9244,'New York','335 Madison Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(9722,'New York','875 Sixth Ave',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10279,'New York','1380 Sixth Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10396,'New York','4 W. 21st Street',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10608,'New York','1021 Third Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10693,'New York','731 Lexington Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10751,'New York','455 Main Street',40.76,-73.95,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10784,'New York','373 5th Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(10841,'New York','655 Lexington Avenue',40.76,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11650,'New York','518 Hudson Street',40.73,-74.01,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11736,'New York','425 Madison Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11737,'New York','340 Madison Avenue',40.75,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11738,'New York','1889 Broadway',40.77,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11764,'New York','350 Fifth Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(11765,'New York','1411 Sixth Avenue',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(13455,'New York','220 East 42nd St',40.75,-73.97,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(13538,'New York','315 Seventh Avenue',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(13539,'New York','14 W. 23rd St',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(13791,'New York','1 Penn Plaza, Concourse level',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14091,'New York','151 W. 34th Street',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14092,'New York','151 W. 34th Street',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14241,'New York','1185 Avenue of the Americas',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14441,'New York','2195 Frederick Douglas Boulevard',40.81,-73.95,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14442,'New York','240 Park Avenue South',40.74,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(14618,'New York','229 Seventh Avenue',40.74,-74.00,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(15388,'New York','270 Park Ave',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(15440,'New York','1101 - 1109 Avenue of the Americas',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(15461,'New York','122 Greenwich Avenue, (space A)',40.74,-74.00,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(15685,'New York','1281 First Avenue',40.77,-73.96,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(16576,'New York','977 Avenue of the Americas',40.75,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(16628,'New York','1140 Broadway',40.74,-73.99,'2017-12-07 01:42:27','2017-12-07 01:42:27'),
	(75589,'New York','1535 Broadway',40.76,-73.99,'2017-12-07 01:42:28','2017-12-07 01:42:28'),
	(75847,'New York','1335 Avenue of the Americas',40.76,-73.98,'2017-12-07 01:42:28','2017-12-07 01:42:28');

/*!40000 ALTER TABLE `starbucks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
