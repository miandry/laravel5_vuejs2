<?php

use Illuminate\Database\Seeder;
use App\Starbucks;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path('data/starbucks_new_york.json');
    	$content = json_decode(file_get_contents($path), true);
    	foreach ($content as $key => $value) {
    		Starbucks::create([
    	     'id' => $value['id'],		
             'city'=>$value['city'],
             'street' => $value['street'],
             'latitude'=>$value['location']['latitude'],
             'longitude'=>$value['location']['longitude'],
            ]);
    	}

    }
}
