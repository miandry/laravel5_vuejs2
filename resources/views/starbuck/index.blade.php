<!DOCTYPE html>
<html>
<head>
  <title>Welcome to Vue</title>
   <!-- Required Stylesheets -->
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
  <link href="css/app.d551159dac9c9e89f69abc1756a2283e.css" rel=stylesheet>
</head>
<body>
   	 <input type="hidden" id="circle" value="2" />
     <input type="hidden" id="user_latitude" value="{{$variables["random"]["latitude"]}}" />
     <input type="hidden" id="user_longitude" value="{{$variables["random"]["longitude"]}}" />
     <input type="hidden" id="base_url" value="http://vuejs.e-roso.net/" />
   
     <div id=app></div>
     <script type="text/javascript" src="js/manifest.c3af6063d7c25b88473e.js"></script>
     <script type="text/javascript" src="js/vendor.cd1fa190d82d90b330dd.js"></script>
     <script type="text/javascript" src="js/app.30a8782517cfac632699.js"></script>
 
</body>
</html>